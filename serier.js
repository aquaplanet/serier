var DailyComic = require("daily-comic.js");
var Hipchatter = require('hipchatter');
var keys = require("./keys.json");
var hipchatter = new Hipchatter(keys.user);

var comic = new DailyComic({
    updateInterval: 3 * 6,
    subscriptions: [ "xkcd", "dilbert" ],
});

comic.on("error", function(error) {
    console.log("error", error);
});

comic.on("new", function(comic, data) {

    /* Example:    console.log("new", comic, data);

    new xkcd { date: Fri Mar 13 2015 04:00:00 GMT+0000 (UTC),
  title: 'Terry Pratchett',
  url: 'http://imgs.xkcd.com/comics/terry_pratchett.png',
  text: 'Thank you for teaching us how big our world is by sharing so many of your own.',
  link: 'http://xkcd.com/1498/' } */

    var token;
    var color;
    var message;
    if (comic === 'dilbert') {
        color = 'yellow';
        token = keys.dilbert;

        message = '<a href="' + data.link + '"/><img src="' + data.url + '"/></a>';
    } else {
        color = 'green';
        token = keys.xkcd;

        message = '<a href="' + data.link + '"/><strong>' + data.title + '</strong><p/><p/><img src="' + data.url + '"/><br/>' + data.text + '</a>';
    }


    hipchatter.notify(keys.room, { message: message, token: token, color: color }, function (err) {
        if (err === null) { console.log("Message was sent successfully!", "new", comic, data); }
    });
});
